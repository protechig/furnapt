<?php 
	if (!empty($_SERVER['SCRIPT_FILENAME']) && 'comments.php' == basename($_SERVER['SCRIPT_FILENAME']))
		die ('Please do not load this page directly. Thanks!');

	if ( post_password_required() ) { ?>
		<p class="nocomments">This post is password protected. Enter the password to view comments.</p>
	<?php
		return;
	}
?>
<div id="comments">

	<h4 id="comments"><?php comments_number('No Comments', '1 Comment', '% Comments' );?></h4>
	
	<ol class="commentlist">
		<?php wp_list_comments('type=comment&callback=format_comment'); ?>
	</ol>
		
	<form id="commentform" action="<?php echo get_option('siteurl'); ?>/wp-comments-post.php" method="post">
		<h4>Leave a comment</h4>
		<p>
			<label for="author">Your Name:</label>
			<input type="text" id="author" class="text" name="author" size="22"  />
		</p>
		<p>
			<label for="email">Email Address:</label>
			<input type="text" id="email" class="text" name="email" />
			<span class="info">*will not be made public</span>
		</p>
		<p>
			<label for="url">Website:</label>
			<input type="text" id="url" class="text" name="url" />
			<span class="info">*optional **without "http://"</span>
		</p>
		<p>
			<label for="comment">Your comment:</label>
			<textarea id="comment"  name="comment" cols="58" rows="10" ></textarea>
		</p>
		<p>
			<input type="submit" id="submit" value="Post Comment" />
			<?php comment_id_fields(); ?>
		</p>
		<?php do_action('comment_form', $post->ID); ?>
	</form>
</div>