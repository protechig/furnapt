<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title><?php wp_title (""); ?></title>
		
		<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" />
		<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/colorbox.css" />
		
		<?php wp_enqueue_script( 'jQuery', get_bloginfo('template_url').'/js/jquery-1.3.2.min.js');?>
		<?php wp_enqueue_script( 'jQueryCycle', get_bloginfo('template_url').'/js/jquery.cycle.all.min.js',array('jQuery'));?>
		<?php wp_enqueue_script( 'jQueryHoverIntent', get_bloginfo('template_url').'/js/jquery.hoverIntent.minified.js',array('jQuery'));?>
		<?php wp_enqueue_script( 'superFish', get_bloginfo('template_url').'/js/superfish.js',array('jQuery'));?>
		<?php wp_enqueue_script( 'custom', get_bloginfo('template_url').'/js/custom.js',array('jQuery'));?>
		
		<?php wp_head();	?>

<!--[if lt IE 7]>
			<script type="text/javascript" src="<?php bloginfo('template_url')?>/js/DD_belatedPNG_0.0.8a.js"></script>
			<script type="text/javascript">DD_belatedPNG.fix('#header,#logo img, #call-to-action, #call-to-action span, #nav a, #cta-bar .icon, #content, #slider img, input, textarea, .category, .comments, .date, #newsletter-address, #newsletter-submit, #twitt-nav div, #testemonial-widget blockquote, #testemonial-widget .by-line, #portfolio .meta, #prices td img','#footer','ul li');</script>
		<![endif]-->

	</head>
<!--[if IE 7]><body class="ie7 ie67"><![endif]-->
<!--[if IE 6]><body class="ie6 ie67"><![endif]-->
<!--[if !IE]>-->
	<body> 
<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-TXJK4X"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-TXJK4X');</script>
<!-- End Google Tag Manager -->
<!--<![endif]-->
     	<div id="wrap">
			<div id="header">
				<div class="container">
					<?php if(get_option('logourl')):?>
						<a href="<?php bloginfo('wpurl');?>" id="logo"><img src="<?php echo get_option('logourl'); ?>" alt="<?php bloginfo('nme');?>"/></a>
					<?php endif; ?>
					<div id="title">
						<a href="<?php bloginfo('wpurl');?>"><?php bloginfo('nme');?></a>
					</div>
					<span id="slogan"><?php echo get_option('blogdescription');?></span>
				</div>
				<?php if(get_option('show-cta-button') == "true"):?>
					<a href="<?php echo get_option('cta-button-href')?>" id="call-to-action"><span><?php echo get_option('cta-button-text')?></span></a>
				<?php endif;?>
				<ul id="nav">
					<li><a href="<?php bloginfo('wpurl');?>">Home</a></li>
					<?php wp_list_pages('title_li=&exclude='.get_option('exclude-from-nav'));	?>
				</ul>
			</div> <!--#header-->
			<div id="content">