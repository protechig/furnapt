<?php get_header();	?>

<h1 class="page-title">Error 404</h1>
<div id="main" class="col span-2-3">
  <h3>We are sorry, the page you are looking for does not exist.</h3>
  <p>Please check to make sure that the page you are looking for is entered correctly.  You may also use the navigation menu above or the search form below to find what you are looking for.</p>
  <p>
    <?php get_search_form(); ?>
  </p>
  <p>If you think you have reached this page in error and there is a problem with the site, or if you have any questions about our services, please feel free to <a href="http://www.furnapt.com/contact/">contact us!</a></p>
</div>
<!-- #main -->
<!-- sidebar -->
<ul id="sidebar" class="column1">
  <li id="text-3" class="widget widget_text">
    <h4 class="widgettitle">Make a Reservation Inquiry</h4>
    <div class="textwidget">
      <div class="wpcf7" id="wpcf7-f1-w1-o1">
        <form action="/locations/401-s-detroit-st/#wpcf7-f1-w1-o1" method="post" class="wpcf7-form">
          <div style="display: none;">
            <input type="hidden" name="_wpcf7" value="1" />
            <input type="hidden" name="_wpcf7_version" value="2.2" />
            <input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f1-w1-o1" />
          </div>
          <div style="padding: 8px; background-color: #ffffaa;">
            <p><b>Name</b> <span style="color: red;">*</span> <span class="wpcf7-form-control-wrap name">
              <input type="text" name="name" value="" class="wpcf7-validates-as-required" size="25" />
              </span> </p>
            <p><b>Email</b> <span style="color: red;">*</span> <span class="wpcf7-form-control-wrap email">
              <input type="text" name="email" value="" class="wpcf7-validates-as-email wpcf7-validates-as-required" size="25" />
              </span> </p>
            <p><b>Phone</b> <small><i>(ex. 555-555-5555)</i></small> <span class="wpcf7-form-control-wrap phone">
              <input type="text" name="phone" value="" size="12" maxlength="12" />
              </span> </p>
            <p><b>How long would you like to rent?</b> <span style="color: red;">*</span> <br />
              <span style="display: block;"><span class="wpcf7-form-control-wrap length"><span class="wpcf7-validates-as-required wpcf7-checkbox"><span class="wpcf7-list-item">
              <input type="checkbox" name="length[]" value="1month" />
              &nbsp;<span class="wpcf7-list-item-label">1month</span></span><span class="wpcf7-list-item">
              <input type="checkbox" name="length[]" value="2months" />
              &nbsp;<span class="wpcf7-list-item-label">2months</span></span><span class="wpcf7-list-item">
              <input type="checkbox" name="length[]" value="3months+" />
              &nbsp;<span class="wpcf7-list-item-label">3months+</span></span><span class="wpcf7-list-item">
              <input type="checkbox" name="length[]" value="With possible extension" />
              &nbsp;<span class="wpcf7-list-item-label">With possible extension</span></span></span></span></span></p>
            <p><b>Number of adults</b> <span style="color: red;">*</span> <br />
              <span class="wpcf7-form-control-wrap adults"><span class="wpcf7-radio"><span class="wpcf7-list-item">
              <input type="radio" name="adults" value="1" />
              &nbsp;<span class="wpcf7-list-item-label">1</span></span><span class="wpcf7-list-item">
              <input type="radio" name="adults" value="2" />
              &nbsp;<span class="wpcf7-list-item-label">2</span></span><span class="wpcf7-list-item">
              <input type="radio" name="adults" value="3" />
              &nbsp;<span class="wpcf7-list-item-label">3</span></span></span></span></p>
            <p><b>Add a message</b><br />
              <span class="wpcf7-form-control-wrap message">
              <textarea name="message" cols="26" rows="2"></textarea>
              </span> </p>
            <p>
              <input type="submit" value="Send" />
              <img class="ajax-loader" style="visibility: hidden;" alt="ajax loader" src="http://www.furnapt.com/wp-content/plugins/contact-form-7/images/ajax-loader.gif" /></p>
          </div>
          <div class="wpcf7-response-output wpcf7-display-none"></div>
        </form>
      </div>
    </div>
  </li>
</ul>
<!-- /sidebar -->
<?php get_footer();	?>
