<?php get_header();	?>

<?php if (have_posts()) : while (have_posts()) : the_post();?>
	
	<h2 class="page-title"><?php the_title();	?></h2>
	<div id="main" class="col span-1-1">
		<div class="blog-post">
			<div class="post-content">
				<?php the_content();	?>
			</div>
		</div><!-- .blog-post -->	
	</div><!-- #main -->	
<?php endwhile; endif; ?>
	
<?php get_footer();	?>