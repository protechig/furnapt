<?php if(get_option('show-cta-bar')):?>
<div id="cta-bar">
	<?php for($i=1;$i<4;$i++){
		$class = '';
		if($i==1) $class = 'first';
			else if($i==3) $class = 'last';
	?>
		<div class="item <?echo $class; ?>"> 
			<a href="<?php echo get_option('cta-bar-href-'.$i);?>">
				<img class="icon" src="<?php echo str_replace("\\","/",get_settings("cta-bar-icon-".$i));?>" alt="" />
				<span class="title"><?php echo get_settings("cta-bar-heading-".$i);?></span>
				<span class="description"><?php echo get_settings("cta-bar-description-".$i);?></span>
			</a>
		</div>
	<?php } ?>
</div> <!-- #cta-bar -->
<?php endif;	?>