<?

$cat = get_the_category(); //retrieving an array of all the categories the current post is in
$cat = $cat[0]->category_nicename; //geting the first category

if (file_exists(TEMPLATEPATH."/single-cat-$cat".".php")) //check is a custom template for this post exists
	include(TEMPLATEPATH."/single-cat-$cat".".php"); //if so use it

else { //else use default
?>

<?php get_header();	?>

<div class="nav-next"><?php next_post_link('%link', ' <span class="meta-nav">&nbsp;&nbsp;</span>', true) ?></div>

<?php if (have_posts()) : while (have_posts()) : the_post();?>
	
	<?php 
	//Getting the top parent category name (ex: Blog)
		$cat = get_the_category();
		$catList = get_category_parents($cat[0],false,',');
		$catListArray = split(",",$catList);
		$title = $catListArray[0]; 
	?>
	
	<div id="main" class="column2">
		<div class="blog-post">
			<h1><a href="<?php the_permalink();	?>"><?php the_title();	?></a></h1>
			<div class="meta">
				<span class="date"><?php the_time('F j, Y');?></span>
				<span class="category"><?php the_category(',');?></span>
				<span class="comments"> <?php comments_popup_link('No Comments', '1 Comment', '% Comments'); ?></span>
			</div> <!-- .meta -->
			<div class="post-content">
				<?php the_content();	?>
			</div>
		</div><!-- .blog-post -->
		
		<?php comments_template(); ?>
		
	</div><!-- #main -->	
<?php endwhile; endif; ?>
	
<?php get_sidebar();	?>
	
<?php get_footer();	?>

<?php }	?>