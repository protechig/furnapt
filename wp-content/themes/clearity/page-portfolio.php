<?php 
/*
 * Template Name: Portfolio
 */
?>
<?php get_header();	?>
<?php if (have_posts()) : the_post();?>
	<h1 class="page-title"><?php echo the_title();?></h1>
	<div id="portfolio">
		<?php
			$catObj = get_category_by_slug($post->post_name);	
			query_posts('cat='.$catObj->term_id);
		?>
		<?php if (have_posts()) : while(have_posts()): the_post();?>
		<div class="item">
			
			<?php
				$link = get_post_meta($post->ID, 'link_to', true);
					
				if($link){
					if(preg_match('/\.jpg|jpeg|gif|png$/i', $link))	$linkType = "image"; 
						else $linkType = "external";
				}
				else{
					$link = get_permalink();
					$linkType = "internal";
				}
			?>
		
			<?php if ( has_post_thumbnail() ) : ?>
				<a href="<?php echo $link;?>" title="<?php the_title();?>">
					<?php the_post_thumbnail(array('250px','150px'),array('class'=>'thumb'));	?>
				</a>
			<?php else: ?>
				<a href="<?php echo $link;?>" title="<?php the_title();?>">
					<img src="" width="250px" height="150px" class="thumb"/>
				</a>
			<?php endif;?>
			<div class="meta">


				<h2>
					<a href="<?php echo $link;?>"><?php the_title();	?></a>
				</h2>
				<a href="<?php echo $link?>" class="more <?php echo $linkType?>"><?php echo $linkType;?></a>
			</div>
		</div><!-- .blog-post -->
		<?php endwhile; endif; ?>
		
		<div id="pagination">
			 <div class="left"><?php previous_posts_link( "Newer" ); ?> </div>
			 <div class="right"><?php next_posts_link( "Older"); ?> </div>
		</div>
		
	</div><!-- #main -->
	
<?php endif;	?>

<?php wp_reset_query(); ?>
	
<?php get_footer();	?>