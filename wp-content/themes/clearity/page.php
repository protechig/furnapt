
<?php get_header();	?>

<?php if (have_posts()) : while (have_posts()) : the_post();?>

	<h1 class="page-title"><?php the_title();	?></h1>
	<div id="main" class="col span-2-3">
		<?php the_content();	?>
	</div><!-- #main -->	
<?php endwhile; endif; ?>
	
<?php get_sidebar();	?>
	
<?php get_footer();	?>