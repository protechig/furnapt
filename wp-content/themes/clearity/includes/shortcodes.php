<?php 
/**
 * @package WordPress
 * @subpackage Clearity-WP
 */

function column_shortcode_handle($atts, $content = ull){
	
	extract(shortcode_atts(array( 
		'pos' => '',
		'size' => '1-3'
	), $atts));
	
	$size = 'span-'.$size;
	$class = 'col '.$pos.' '.$size;
	
	return  '<div class="'.$class.'">'.do_shortcode(clean_column_content($content)).'</div><!-- end of .column -->';
}
	
add_shortcode('col', 'column_shortcode_handle');

function posts_feed_shortcode_handle($atts, $content=null){

	extract(shortcode_atts(array(
		'category' => '',
		'limit' => 10,
	), $atts));
	
	$category = get_cat_id($category);

	$posts = get_posts(array('numberposts' => $limit, 'category' => $category));
	
	$output = '<ul class="posts-feed">';
	
	foreach($posts as $post){
		
		$output .= '<li>
				<!--<span class="date">'.get_post_time('n/j/y', false, $post, true).'</span>--><a href="'.get_permalink($post->ID).'">'.$post->post_title.'</a>
		</li>
		';
	
	}
	
	$output .= '</ul>';
	
	return $output;
}
	
add_shortcode('posts_feed', 'posts_feed_shortcode_handle');

function box_shortcode_handle($atts, $content=null){
	
	extract(shortcode_atts(array(
		'style' => '',
		'align' => '',
		'width' => ''
	), $atts));
	
	if($width != '') $width = ' style="width:'.$width.'"';
	
	if($align) $align = ' align'.$align;
	
	return '<div class="box '.$style.$align.'"'.$width.'>'.apply_filters('the_content', rtrim($content,'<p></p>')).'</div>';
	
}

add_shortcode('box', 'box_shortcode_handle');

function list_shortcode_handler($atts, $content=null){
	
	extract(shortcode_atts(array(
		'type' => '',
	), $atts));
	
	return str_replace('<ul>','<ul class="'.$type.'">', $content);
}

add_shortcode('list', 'list_shortcode_handler');

function clearity_button($atts, $content = null){

	extract(shortcode_atts(array(
		"text" => '',
		"href" => '#',
		"color" => '',
	), $atts));
	
	return '<a class="button '.$color.'" href="'.$href.'">'.$text.'</a>';
}

add_shortcode('button','clearity_button');
?>