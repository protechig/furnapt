<?php 

function format_comment($comment, $args, $depth){
	$GLOBALS['comment'] = $comment; ?>
	
	<div class="comment">
		<div class="meta">
			<?php echo get_avatar($comment,$size='80',$default='' ); ?>
			<span class="author">
				<a href="<?php comment_author_url(); ?>"><?php comment_author();?></a>
				<small> said</small>
			</span>
			<span class="time"><?php comment_date()?></span>
		</div>
		<div class="content">
			<?php comment_text() ?>
		</div>
	</div>
	
<?php 
}
?>