<?php 
/*
 * Various functions I'll use to build the theme option
 */
 
/*
 * Removes the 
 */
function clean_column_content($content){
	if(substr($content,0,4) == '</p>') $content = substr($content,4);
	
	if(substr($content,-3) == '<p>') $content = substr($content,0, -3);
	
	return $content;
}


/*
 * Return an array containg all pages and all categories 
 */
function getHomepageSubpagesAndCategories(){

	$homepage = get_settings('page_on_front');
	$pages = get_pages('child_of='.$homepage);
	$categories = get_categories();
	
	$result = array();
	
	$result[] = array( 'value' => "", 'name' => '__PAGES__', 'disabled' => true);
	
	foreach($pages as $p){
		$result[] = array(
			'value' => 'page-'.$p->ID,
			'name' => $p->post_title
		);
	}
	
	$result[] = array( 'value' => "", 'name' => '__CATEGORIES__', 'disabled' => true);
	
	foreach($categories as $p){
		$result[] = array(
			'value' => 'category-'.get_cat_id($p->cat_name),
			'name' => $p->cat_name
		);
	}
	
	return $result;
}

/*
 * Return am array of categories formated for the admin page builder
 */
function getCategories(){
	$categories = get_categories();
	$results = array();
	$results[] = array('value'=>'','name'=>'Choose category...	','disabled'=>true);

	foreach($categories as $cat){
		$results[] = array(
				'value' => get_cat_id($cat->cat_name),
				'name' => $cat->cat_name,
			);
	}
	
	return $results;
}

/*
 * Returns the icon found in TEMPLATEPATH/img/icons
 */
function getCallToActionIcons(){
	$dir = TEMPLATEPATH."/img/icons/";

	$icons = array();
	
	if($handle = opendir($dir)){
		while( ($file = readdir($handle)) !== false){
			if(is_file($dir.$file)){
				$icon['name'] = $file;
				$icon['value'] = get_bloginfo('template_url').'/img/icons/'.$file;
				$icons[] = $icon;
			}
		}
		return $icons;
	}
	return null;
}
?>