<?php 
function clearity_register_sidebars(){
	
	$sidebars = array('Default', 'Default Page', 'Blog Post', 'Footer');

	$pagesSidebars = explode(',', trim(get_option('clearity_sidebars')));
	
	foreach($pagesSidebars as $ps){
		$sidebars[] = $ps;
	}

	foreach($sidebars as $sidebar){
		
		if($sidebar != "")
			register_sidebar(array(
				'name' => $sidebar,
			));
		
	}
	
}

function get_clearity_sidebar(){
	
	global $post;
	
	if(is_page()){
		
		$result = get_post_meta($post->ID, 'sidebar', true);
		
		return $result?$result:'Default Page';
	}
	if(is_single()){
		return 'Blog Post';
	}
	
	return 'default';
}
?>