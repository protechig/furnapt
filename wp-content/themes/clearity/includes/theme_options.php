<?php 
require_once(TEMPLATEPATH.'/includes/utilities.php');

add_action('admin_menu', 'clearity_add_admin');


$options = array(
	array(	"type" => "title",
			"name" => "General settings",
	),
	array(	"type" => "open"	),
	array(	"type" => "text",
			"id" => "blogname",
			"name" => "Site title",
			"description" => 'The title of the site. This will OVERWRITE the "Blog Title" field from the Settings->General page.'
	),
	array(	"type" => "text",
			"id" => "blogdescription",
			"name" => "Tagline",
			"description" => 'The text shown under the title. This will OVERWRITE the "Tagline" field from the Settings->General page.'
	),
	array(	"type" => "text",
			"id" => "logourl",
			"name" => "Logo Path",
			"description" => 'The path to the logo image. Leave empty if you dont\'t want a logo to be shown. (Recommended height around 50px)',
	),

	array(	'type' => 'checkbox',
			'id' => 'show-cta-button',
			'name' => 'Show call to action button',
			'description' => 'Check is you want to show the CTA button (Upper right corner button)',
	),
	array(	"type" => 'text',
			'id' => 'cta-button-text',
			'name' => 'CTA button text',
			'description' => 'The text for the CTA button',
	),
	array( 'type' => 'text',
			'id' => 'cta-button-href',
			'name' => 'Call to action button link',
			'description' => 'The link to which the CTA button points.',
	),
	array( 'type' => 'text',
			'id' => 'exclude-from-nav',
			'name' => 'Exclude from navigation',
			'description' => 'Comma separated list of page ID\'s to be excluded from navigation.',
	),
	array( 	"type" => "close"	),
	
	array(	"type" => "title",
			"name" => "Call to action bar"
	),
	array(	'type' => 'open'),
	array(	'type' => 'checkbox',
			'id' => 'show-cta-bar',
			'name' => 'Show call to action bar',
			'description' => 'Check if you want the Call To Action Bar to be showed',
	),
	array(	'type' => 'close'),
	
	//First button
	array(	"type" => "subtitle",
			"name" => "First Button"
	),
	array(	"type" => "open"	),	
		array(	"type" => "select",
				"id" => "cta-bar-icon-1",
				"name" => "Icon",
				"description" => "Select the icon you want to be shown for the first button in the bar.",
				"values" => getCallToActionIcons(),
		),
		array(	"type" => "text",
				"id" => "cta-bar-heading-1",
				"name" => "Heading",
		),
		array(	"type" => "text",
				"id" => "cta-bar-description-1",
				"name" => "Description",	
		),
		array(	"type" => "text",
				"id" => "cta-bar-href-1",
				"name" => "Link url",
				"description" => "The url to which the button points."
		),
	array(	"type" => "close"	),
	//second button	
	array(	"type" => "subtitle",
			"name" => "Second Button"
	),
	array(	"type" => "open"	),
		array(	"type" => "select",
				"id" => "cta-bar-icon-2",
				"name" => "Icon",
				"description" => "Select the icon you want to be shown for the first button in the bar.",
				"values" => getCallToActionIcons(),
		),
		array(	"type" => "text",
				"id" => "cta-bar-heading-2",
				"name" => "Heading",
		),
		array(	"type" => "text",
				"id" => "cta-bar-description-2",
				"name" => "Description",	
		),
		array(	"type" => "text",
				"id" => "cta-bar-href-2",
				"name" => "Link url",
				"description" => "The url to which the button points."
		),
	array(	"type" => "close"	),
	//third button	
	array(	"type" => "subtitle",
			"name" => "Third Button"
	),
	array(	"type" => "open"	),
		array(	"type" => "select",
				"id" => "cta-bar-icon-3",
				"name" => "Icon",
				"description" => "Select the icon you want to be shown for the first button in the bar.",
				"values" => getCallToActionIcons(),
		),
		array(	"type" => "text",
				"id" => "cta-bar-heading-3",
				"name" => "Heading",
		),
		array(	"type" => "text",
				"id" => "cta-bar-description-3",
				"name" => "Description",	
		),
		array(	"type" => "text",
				"id" => "cta-bar-href-3",
				"name" => "Link url",
				"description" => "The url to which the button points."
		),
	array(	"type" => "close"	),
	array(	"type" => "title",
			"name" => "Slider",
	),
	array(	"type" => "open"),
	array(	"type" => "select",
			"id" => "slider-category",
			"name" => "Slider category:",
			"description" => "Select the name of the category you want to be displayed in the slider.",
			"values" => getCategories(),
	),
	array(	"type" => "text",
			"id" => "slider-item-number",
			"name" => "Item Number",
			"description" => 'Enter the number of items the slider will contain. "0" or blank to show all posts from the "Slider category". '
	),
	array(	"type" => "select",
			"id" => "slider-order",
			"name" => "Order slides",
			"description" => 'Select the order in which you want your slider to be displayed',
			"values" => array(
				array('name'=>'Ascendent', 'value' => 'ASC'),
				array('name'=>'Descendent', 'value' => 'DESC'),
				array('name'=>'Random', 'value' => 'RAND'),
			),
	),
	
	array(	"type" => "checkbox",
			"id" => "slider-show-titles",
			"name" => "Show titles",
			"description" => "Check if you want the post title to be displayed in the slider."
	),
	array(	'type' => 'select',
			'id' => 'slider-effect',
			'name' => 'Transition Effect',
			'description' => 'Select the transition effect for the home page slider',
			'values' => array(
				array('name'=>'Fade', 'value' => 'fade'),
				array('name'=>'Scroll Down', 'value' => 'scrollDown'),
				array('name'=>'Scroll Up', 'value' => 'scrollUp'),
				array('name'=>'Scroll Left', 'value' => 'scrollLeft'),
				array('name'=>'Scroll Right', 'value' => 'scrollRight'),
				array('name'=>'Slide X', 'value'=>'slideX'),
				array('name'=>'Slide Y', 'value'=>'slideY'),
				array('name'=>'Toss', 'value'=>'toss'),
				array('name'=>'Turn Up', 'value'=>'turnUp'),
				array('name'=>'Turn Down', 'value'=>'turnDown'),
				array('name'=>'Turn Left', 'value'=>'turnLeft'),
				array('name'=>'Turn Right', 'value'=>'turnRight'),
				array('name'=>'Uncover', 'value'=>'uncover'),
				array('name'=>'Wipe', 'value'=>'wipe'),
			),
	),
	array(	'type' => 'text',
			'id' => 'slider-timeout',
			'name' => 'Transition Timeout',
			'description' => 'Milliseconds between transitions',
	),
	array(	'type' => 'text',
			'id' => 'slider-speed',
			'name' => 'Transition Speed',
			'description' => 'The speed of the transition,in miliseconds',
	),
	array(	'type' => 'checkbox',
			'id' => 'slider-autopause',
			'name' => 'Pause on hover',
			'description' => 'Check if you want the slider to stop when it\' in hover state',
	),
	array(	"type" => "close"),
	
	array(	'type' => 'title',
			'name' => 'Sidebars'		
	),
	array(	'type' => 'open'),
	
	array(	'type' => 'textarea',
			'id' => 'clearity_sidebars',
			'name' => 'Sidebars list',
			'description' => 'comma separated list of all the sidebars you need',
	),
	
	array(	'type' => 'close'),
	
	array( 	"type" => 'title',
			'name' => 'Footer'		
	),
	array(	'type' => 'open'),
		array(	'type' => 'text',
				'id' => 'copyright-text',
				'name' => 'Copyright Text',
				'description' => 'The text to be displayed in right part of the footer',
		),
	array(	'type' => 'close'),
);

function clearity_add_admin(){
	if ( $_GET['page'] == basename(__FILE__) ) {
		
		if($_REQUEST['action'] == 'save') save_settings();
			
	}
	
	add_theme_page("Clearity Options", "Clearity Options", 'edit_themes', basename(__FILE__), 'clearity_build_admin');
}

function clearity_build_admin(){
	
	global $options;
	
	if ( $_REQUEST['saved'] ) echo '<div id="message" class="updated fade"><p><strong>Settings saved.</strong></p></div>';
    if ( $_REQUEST['reset'] ) echo '<div id="message" class="updated fade"><p><strong>Settings reset.</strong></p></div>';	?>

	<div class="wrap">
		<div id="icon-options-general" class="icon32"><br/></div>
		<h2>Clearity Options</h2>
		<form method="post">
	<?php foreach($options as $option){
			
			switch($option['type']){
		
				case 'open':
					echo '<table class="form-table">';
					break;
				
				case 'close':
					echo '</table>';
					break;
				
				case 'title':
					echo '<h3>'.$option['name'].'</h3>';
					break;
					
				case 'subtitle':
					echo '<h4>'.$option['name'].'</h4>';
					break;
					
				case 'text': ?>
					<tr>
						<th><?php echo $option['name']; ?></th>
						<td>
							<input id="<?php echo $option['id'];?>" name="<?php echo $option['id'];?>" type="text" style="width: 200px;"
								value="<?php if( get_option($option['id']) != "") echo get_option($option['id']); else { echo $option['std']; } ?>"/>
							<span class="description"><?php echo $option['description']?></span>
						</td>
					</tr>
				<?php 
					break;
				case 'textarea': ?>
					<tr>
						<th><?php echo $option['name']; ?></th>
						<td>
							<textarea id="<?php echo $option['id']?>" name="<?php echo $option['id']?>" style="width: 200px;" cols="50" rows="5"><?php if(get_option($option['id'])) echo get_option($option['id']); ?></textarea>
						</td>
					</tr>
				<?php
					break;
				case 'checkbox': ?>
					<tr>
						<th><?php echo $option['name']; ?></td>
						<td>
							<? if(get_settings($option['id'])){ $checked = "checked=\"checked\""; }else{ $checked = ""; } ?>
							<input type="checkbox" name="<?php echo $option['id']; ?>" id="<?php echo $option['id']; ?>" value="true" <?php echo $checked; ?> />
							<span class="description"><?php echo $option['description']?></span>
						</td>
					</tr>	
				<?php 
					break;
				case 'select': ?>
					<tr>
						<th><?php echo $option["name"];	?></th>
						<td>
							<select id="<?php echo $option['id'];?>" name="<?php echo $option['id'];?>" style="width: 200px;">
								
							<?php foreach($option["values"] as $field){ ?>

								<option value="<?php echo $field['value']?>" <?php if($field['disabled']) echo 'disabled="disabled"'; if(get_settings($option['id']) == $field['value']) echo ' selected="selected"'?>>
									<?php echo $field['name']?>
								</option>
							<?php }?>								
							</select>
							<span class="description"><?php echo $option['description']?></span>
						</td>
					</tr>
					<?php
					break;
			}			
		}
		
	
		?>
		<p class="submit">
			<input class="button-primary" id="save" name="save" type="submit" value="Save changes" />    
			<input type="hidden" name="action" value="save" />
		</p>
		</form>
	</div> <!-- #wrap -->   
<?php
}

function save_settings(){
	
	global $options;
	
	foreach ($options as $value) {
		update_option( $value['id'], $_REQUEST[ $value['id'] ] ); }

	foreach ($options as $value) {
		if( isset( $_REQUEST[ $value['id'] ] ) ) { update_option( $value['id'], $_REQUEST[ $value['id'] ]  ); } else { delete_option( $value['id'] ); } }

		
	
}

?>