
<?php get_header();	?>

	<div id="main" class="col span-2-3">
		
	<?php query_posts($query_string . '&cat='); ?>
	<?php if (have_posts()) : while(have_posts()): the_post();?>
		<div class="blog-post excerpt">
			<!--<?php if ( has_post_thumbnail() ) : ?>
				<a href="<?php the_permalink();?>" title="<?php the_title();?>">
					<?php the_post_thumbnail(array(150,150),array('class'=>'thumb'));	?>
				</a>
			<?php else: ?>
				<a href="<?php the_permalink();?>" title="<?php the_title();?>">
					<img src="" width="150px" height="150px" class="thumb"/>
				</a>
			<?php endif;?>-->
			<h2><a href="<?php the_permalink();	?>"><?php the_title();	?></a></h2>
			<div class="meta">
				<span class="date"><?php the_time('F j, Y');?></span>
				<span class="category"><?php the_category(',');?></span>
				<span class="comments"> <?php comments_popup_link('No Comments', '1 Comment', '% Comments'); ?></span>
			</div> <!-- .meta -->
			<div class="post-content">
				<?php the_excerpt();	?>
				<a class="read-more" href="<?php the_permalink();?>" >Continue reading</a>
			</div>
		</div><!-- .blog-post -->
		<?php endwhile; endif; ?>
		
		<div id="pagination">
			 <?php previous_posts_link( "Newer" ); ?>
			 <?php next_posts_link( "Older"); ?>
		</div>
		
	</div><!-- #main -->


<?php get_sidebar();	?>
	
<?php get_footer();	?>