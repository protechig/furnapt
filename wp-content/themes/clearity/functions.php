<?php 
require_once(TEMPLATEPATH.'/includes/format_comments.php');

require_once(TEMPLATEPATH.'/includes/theme_options.php');

require_once(TEMPLATEPATH.'/includes/shortcodes.php');

require_once(TEMPLATEPATH.'/includes/sidebars.php');

clearity_register_sidebars();

//Adding support for post thumbnails
add_theme_support( 'post-thumbnails');

add_filter('widget_text', 'do_shortcode');

//Adding support for top bar widget
if ( function_exists('register_sidebar') ) {
    register_sidebar(array( 'name' => 'Top Bar',
        'before_widget' => '<li id="%1$s" class="widget %2$s">',
        'after_widget' => '',
        'before_title' => '<h2 class="widgettitle">',
        'after_title' => '</h2>',
    ));
}
?>