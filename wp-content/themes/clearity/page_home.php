<?php 
/*
 * Template Name: Homepage
 */
?>

<?php get_header();	?>
<?php include(TEMPLATEPATH.'/slider.php');	?>

<?php include(TEMPLATEPATH.'/cta-bar.php')?>

<?php if (have_posts()) : while (have_posts()) : the_post();?>
	<?php if($post->post_content != "") {?>
		<div id="main" class="col span-1-1">
			<?php the_content();	?>
		</div><!-- #main -->
	<?php } ?>
<?php endwhile; endif; ?>
	
<?php get_footer();	?>