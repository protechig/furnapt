<div id="slider">
	<?php
		$sliderCategory = get_option('slider-category');
		$itemsNumber = get_option('slider-item-number');
		if($itemsNumber == 0 || $itemsNumber == FALSE) $itemsNumber = -1;
	?>
	<ul>
		<?php
		 global $post;
		 
		 if(get_option('slider-order') == 'ASC' || get_option('slider-order') == "DESC") $order = '&order='.get_option('slider-order');
		 
		 
		 $sliderPosts = get_posts('numberposts='.$itemsNumber.'&category='.$sliderCategory.$order);
		 foreach($sliderPosts as $post) :	setup_postdata($post);	
		 	$bgImage = get_post_meta($post->ID, 'slide-image', true);
		 	if($bgImage){ $style = 'style="background : url('.$bgImage.') no-repeat; "';	}
		 	else $style = '';
		 ?>
		 	<li <?php echo $style; ?>>
		 	<?php 
		 		if(get_settings('slider-show-titles')){
		 			echo '<h3>'; the_title(); echo '</h3>';
		 		}
		 		the_content();
		 	?>		 	
		 	</li>
		 <?php endforeach; ?>
	</ul> 
	<div id="slider-control"><div id="slider-pager"></div></div>
	<script type="text/javascript">
		$(document).ready(function() {
			$('#slider').find('ul:first').cycle({
				fx: '<?php echo get_option('slider-effect')?>',
				speed: <?php echo get_option('slider-speed')?get_option('slider-speed'):'500'; ?>, 
				timeout:  <?php echo get_option('slider-timeout')?get_option('slider-timeout'):'5000'; ?>,
				random: <?php if(get_option('slider-order') == 'RAND') echo '1'; else echo '0'; ?>,
				pagerAnchorBuilder: pagerAnchorBuilder,
				pager: '#slider-pager',
				pause: <?php if(get_option('slider-autopause')) echo '1'; else echo '0'; ?>,
				height: 'auto',
				width: 960,
				height: 300,
				cleartype: true,
			    cleartypeNoBg: true
			});

		});
	</script>
</div> <!-- #slider -->