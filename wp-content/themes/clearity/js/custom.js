$(document).ready(function() {	
	
	clearity_slider();
	
	clearity_menu();
	
	clearity_portfolio();

 });

function clearity_menu(){
	$('#nav').superfish({
		
	});
}


function clearity_portfolio(){
	$("#portfolio div.meta").hide();
	
	$("#portfolio div.item").hoverIntent(
		function(){
			$("div.meta", this).fadeIn();
		},
		function(){
			$("div.meta", this).fadeOut();
		});

		
}

function clearity_slider(){
	
	var ctrl= $('#slider-control');
	$('#slider').hoverIntent(
			function(){
				ctrl.fadeIn('slow');
			},
			function(){
				ctrl.fadeOut('fast');
			}
	);
}
//builds a link for a slider element, will be shown in the pager element (default='#slider-pager')
function pagerAnchorBuilder(){
	return '<a href="#">&bull;</a>'
}

getTwitters('twitt-bubble', { 
	id: 'clearity_theme', 
	callback: makeTwitterCycle,
	clearContents: true, 
	count: 5, 
	withFriends: false,
	ignoreReplies: false,
	template: '<span class="status">"%text%"</span> <span class="time"><a href="http://twitter.com/%user_screen_name%/statuses/%id%">%time%</a></span>'
});

function makeTwitterCycle(){			
	$("#twitt-bubble ul").cycle({ 
	    fx:      'scrollDown', 
	    speed:    300, 
	    timeout:  10000,
	    next: '#twitt-nav .next',
	    prev: '#twitt-nav .prev'
	});
}